// SampleCriticalSection.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"
#include <windows.h>

int g_count;		// 排他制御するオブジェクト
BOOL g_endThread1;	// スレッド1終了フラグ
BOOL g_endThread2;	// スレッド2終了フラグ

/**
 * @brief スレッド1処理
 *
 * @param [in] param クリティカルセクションオブジェクト
 * @return 処理結果
 */
DWORD WINAPI Thread1(LPVOID* param)
{
	// クリティカルセクションオブジェクトを保持し、排他制御を開始します。
	//EnterCriticalSection((LPCRITICAL_SECTION)param);

	// 排他制御したい処理を記述します。
	// Thread1ではg_countをカウントアップして、結果を表示します。
	while (g_count < 10)
	{
		printf("Thread1 count=%d\n", g_count);
		++g_count;
		Sleep(13);
	}

	// クリティカルセクションオブジェクトを解放し、排他制御を終了します。
	//LeaveCriticalSection((LPCRITICAL_SECTION)param);

	// スレッド終了フラグを設定します。
	g_endThread1 = TRUE;
	return 0;
}

/**
 * @brief スレッド1処理
 *
 * @param [in] param クリティカルセクションオブジェクト
 * @return 処理結果
 */
DWORD WINAPI Thread2(LPVOID* param)
{
	//EnterCriticalSection((LPCRITICAL_SECTION)param);

	while (g_count > 0)
	{
		printf("Thread2 count=%d\n", g_count);
		--g_count;
		Sleep(7);
	}

	//LeaveCriticalSection((LPCRITICAL_SECTION)param);

	// スレッド終了フラグを設定します。
	g_endThread2 = TRUE;
	return 0;
}

/**
 * @brief メイン関数
 *
 * @param [in] argc コマンドライン引数
 * @param [in] argv[] 
 * @return 処理結果
 */
int _tmain(int argc, _TCHAR* argv[])
{
	static CRITICAL_SECTION cs; // クリティカルセクションオブジェクト
	static HANDLE hThread1;		// スレッド1ハンドル
	static HANDLE hThread2;		// スレッド2ハンドル

	// グローバル変数を初期化します。
	g_count = 5;
	g_endThread1 = FALSE;
	g_endThread2 = FALSE;

	// クリティカルセクションを初期化します。
	InitializeCriticalSection(&cs);

	// Thread1, Thread2を生成し、開始します。
	hThread1 = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Thread1, (LPVOID)&cs, 0, NULL);
	hThread2 = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Thread2, (LPVOID)&cs, 0, NULL);

	// Thread1, Thread2の終了を待ちます。
	// 今回は処理を単純化するため、終了フラグが設定されるのをポーリングして待ちます。
	while (TRUE)
	{
		if (g_endThread1 && g_endThread2)
		{
			break;
		}

		Sleep(10);
	}

	// スレッドハンドルをクローズします。
	CloseHandle(hThread1);
	CloseHandle(hThread2);

	// クリティカルセクションを削除します。
	DeleteCriticalSection((LPCRITICAL_SECTION)&cs);

	return 0;
}

